# Blog comments system for AWS Lambda  

Using:
- AWS Lambda
- DynamoDB

### Environments variables:
```
AKISMET_KEY=XXXX # API KEY for Akismet
AKISMET_SITE_URL=https://YYYYY/ # Site URL declared in Akismet
DYNAMODB_TABLE=ZZZZZZZZ # Name of the DynamodDB table
```
More information on blog:  https://blog.devops.in.net/2020-11-04-blog-post-comments/