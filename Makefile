.PHONY: all test clean

help:           ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

clean:
	@rm -fr package
	@rm -fr function.zip

package: clean ## Package dependencies for update-lambda
	@pip install --target ./package python-akismet
	@pip install --target ./package aws-xray-sdk
	@cd package && zip -r9 ${OLDPWD}/function.zip .
	@zip -g function.zip lambda_function.py


update: package
	@aws lambda update-function-code --function-name blog_comment_devops_in_net --zip-file fileb://function.zip