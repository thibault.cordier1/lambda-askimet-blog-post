import json
from os import environ

import boto3
from akismet import Akismet
from boto3.dynamodb.conditions import Key

from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core import patch_all

patch_all()

class Comment:
    ip: str
    user_agent: str
    author_name: str
    author_email: str
    content: str
    article_slug: str
    timestamp: str

    def __init__(self,
                 ip: str,
                 user_agent: str,
                 author_name: str,
                 author_email: str,
                 content: str,
                 article_slug: str,
                 timestamp: str
                 ):
        self.ip = ip
        self.user_agent = user_agent
        self.author_name = author_name
        self.author_email = author_email
        self.content = content
        self.article_slug = article_slug
        self.timestamp = timestamp


class AkismetConfig:
    key: str
    site_url: str

    def __init__(self):
        self.key = environ.get('AKISMET_KEY')
        self.site_url = environ.get('AKISMET_SITE_URL')


def get_askimet_config() -> AkismetConfig:
    return AkismetConfig()


def check_comment(comment: Comment):
    akismet_config = get_askimet_config()
    akismet = Akismet(akismet_config.key, akismet_config.site_url, "Devops.in.net Lambda Moderator")

    response = akismet.check(comment.ip, comment.user_agent, comment_author=comment.author_name,
                             comment_content=comment.content, comment_author_email=comment.author_email)

    if response == 0:
        print("Go write in database")
        return write_in_database(comment)
    else:
        print("Spam detected.. aborting")
        return False


def write_in_database(comment_to_write: Comment):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(environ.get('DYNAMODB_TABLE'))
    table.put_item(
        Item={
            'article_slug': comment_to_write.article_slug,
            'comment_timestamp': comment_to_write.timestamp,
            'author': comment_to_write.author_name,
            'email': comment_to_write.author_email,
            'content': comment_to_write.content,
            'ip': comment_to_write.ip,
            'user_agent': comment_to_write.user_agent
        }
    )
    return True


def fetch_comments_count(article_slug: str):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(environ.get('DYNAMODB_TABLE'))
    response = table.query(
        TableName=environ.get('DYNAMODB_TABLE'),
        Select='COUNT',
        KeyConditionExpression=Key('article_slug').eq(article_slug)
    )
    return response['Count']


def fetch_comments(article_slug: str):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(environ.get('DYNAMODB_TABLE'))
    response = table.query(
        ProjectionExpression="comment_timestamp, author, email, content, article_slug",
        ScanIndexForward=True,
        KeyConditionExpression=Key('article_slug').eq(article_slug)
    )
    return response['Items']


def lambda_handler(event, context):
    print(event)
    ip = event['requestContext']['identity']['sourceIp']
    user_agent = event['requestContext']['identity']['userAgent']
    request_time_epoch = event['requestContext']['requestTimeEpoch']

    response = {
        'headers': {
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
        }
    }

    # Fetch Comments for an article
    if event['path'] == '/comments/' or event['path'] == '/comments':
        # GET comments for an article
        if event['httpMethod'] == 'GET':
            if 'queryStringParameters' in event:
                if event['queryStringParameters'] is None or 'article-slug' not in event['queryStringParameters']:
                    response['statusCode'] = 400
                    response['body'] = json.dumps({
                        'error': 'article slug param is expected'
                    })
                    return response
                else:
                    comments = fetch_comments(event['queryStringParameters']['article-slug'])
                    response['statusCode'] = 200
                    response['body'] = json.dumps(comments)
                    return response

        # POST Add a comment to an article
        if event['httpMethod'] == 'POST':
            if 'body' in event:
                body = json.loads(event['body'])
                if 'author_name' in event['body'] \
                        and 'author_email' in body \
                        and 'content' in body \
                        and 'article-slug' in body:
                    incoming_comment = Comment(ip=ip,
                                               user_agent=user_agent,
                                               author_name=body['author_name'],
                                               author_email=body['author_email'],
                                               content=body['content'],
                                               article_slug=body['article-slug'],
                                               timestamp=str(request_time_epoch)
                                               )
                    if check_comment(incoming_comment):

                        response['statusCode'] = 200
                        response['body'] = json.dumps({
                            'success': 'comment was added :)'
                        })
                        return response
                    else:
                        response['statusCode'] = 400
                        response['body'] = json.dumps({
                            'error': 'spam detected, thanks'
                        })
                        return response
                else:
                    response['statusCode'] = 400
                    response['body'] = json.dumps({
                        'error': 'required values: author_name, author_email, content, article-slug'
                    })
                    return response

    # Fetch Comments for an article
    if (event['path'] == '/comments/count/' or event['path'] == '/comments/count') and event['httpMethod'] == 'GET':

        if 'queryStringParameters' in event:
            if event['queryStringParameters'] is None or 'article-slug' not in event['queryStringParameters']:
                response['statusCode'] = 400
                response['body'] = json.dumps({
                    'error': 'article slug param is expected'
                })
                return response
            else:
                comments_count = fetch_comments_count(event['queryStringParameters']['article-slug'])
                response['statusCode'] = 200
                response['body'] = json.dumps({"count": comments_count})
                return response

    return {
        'statusCode': 200,
        'body': json.dumps('Hello here'),
        'headers': {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
            "Access-Control-Allow-Headers": "Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token"
        }

    }


if __name__ == '__main__':
    fetch_comments_count("blog-post-comments-in-dynamodb")
